const appInsights = require('../Common/insights');

appInsights.initialize();

const client = appInsights.getClient();

const { AzureLog, AzureLogError } = require('../Common/utils');

module.exports = async function azureFunctionMain(context, httpRequest) {
	let returnStatus = 200;
	let returnMessage;
	const { invocationId } = context.executionContext;
	const { headers } = httpRequest;
	try {
		if (!httpRequest.body) {
			AzureLogError('No JSON body provided', context, client);
			throw Error('No JSON body provided');
		}
		returnMessage = Object.assign({}, httpRequest.body);

		client.trackEvent({
			name: 'MessagedReceived',
			properties: { returnMessage, headersFromRequest: headers, invocationId }
		});
		AzureLog(JSON.stringify(returnMessage), context);
	} catch (error) {
		returnStatus = httpRequest.body ? 500 : 400;
		returnMessage = error;

		AzureLogError(error, context, client);
	}

	// it's an accepted practice to set response on the context obj for azure functions
	// eslint-disable-next-line no-param-reassign
	context.res = {
		headers: {
			'Content-Type': 'application/javascript'
		},
		status: returnStatus,
		body: { returnMessage, headersFromRequest: headers }
	};
};
